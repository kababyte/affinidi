import PropertiesReader from 'properties-reader';

const propertiesPath = "conf/config.properties";
const properties = PropertiesReader(propertiesPath, "utf-8", {writer: {saveSections: true}});

export class Config {
    static getProperty(key: string): any {
        return properties.get(key);
    }

    static async updateProperty(key: string, value: string) {
        await properties.set(key, value);
        await properties.save(propertiesPath);
    }
}
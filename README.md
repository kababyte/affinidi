# Affinidi test task

## What is it?

RestAPI test suite done in scope of Affinidi take-home assigment

Tools used:
- jest
- supertest
- allure
- faker


## Structure
 
1. Conf 

         .environment configuration file
         Configuration read and write helper
2. Dto
         
         User sample dto used in test
4. Test

         Split in 2 blocks based on access restrictions


-------------------

## How to run?

### ⚠️Pre run⚠️
Lets rev up our json-server with auth midtier

   `npm start`

### Run tests

1. Run test with default config and generate allure report

   `npm test`


2. Run test with ALBUM_ID variable

   `ALBUM_ID=5 npm test`

### Serve report

   `npm run report`
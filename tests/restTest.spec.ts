import request from "supertest";
import {performance} from 'universal-perf-hooks'
import {User} from '../dto/user';
import {faker} from '@faker-js/faker';
import {Config} from "../conf/config";
import {Severity} from "jest-allure/dist/Reporter";

declare const reporter: any;

const baseUrl = Config.getProperty("baseUrl");
const confUserId = Config.getProperty("userId");
const confPostId = Config.getProperty("postId");
const confAlbumId = process.env.ALBUM_ID || 3;
let accessToken: string;


describe("Verify resources without authorization", () => {
    beforeEach(() => {
        reporter
            .epic("Interview")
            .feature("Querying")
            .severity(Severity.Normal);
    });
    it("Get all posts", async () => {
        reporter
            .story("Get posts");

        const response = await request(baseUrl)
            .get("/posts");

        reporter.startStep("Response status code should be 200");
        await expect(response.statusCode).toBe(200);
        reporter.endStep();

        reporter.startStep("Content type should be 'application/json'");
        await expect(response.get("content-type")).toBe("application/json; charset=utf-8");
        reporter.endStep();
    });
    it(`Get album #${confAlbumId}`, async () => {
        reporter
            .story("Get albums");

        const response = await request(baseUrl)
            .get("/albums").query({
                userID: confUserId,
                id: confAlbumId
            });

        reporter.startStep("Response status code should be 200");
        await expect(response.statusCode).toBe(200);
        reporter.endStep();

        reporter.startStep("Response should have non 0 content length");
        expect(response.body.length).toBeGreaterThan(0);
        reporter.endStep();
    });

    it("Response time on photos", async () => {
        reporter
            .story("Response time");

        reporter.startStep("Response time should be less that 10 seconds");
        const time = performance.now();

        await request(baseUrl)
            .get("/photos")
            .expect(() =>
                expect(performance.now() - time)
                    .toBeLessThan(10 * 1000));

        reporter.endStep();
    });

    it(`Get ${confPostId} post comments and verify ordering`, async () => {
        reporter
            .story("Sorting");

        const response = await request(baseUrl)
            .get("/comments")
            .query({
                postId: confPostId,
                _sort: "id",
                _order: "desc"
            });

        reporter.startStep("Status code should be 200");
        expect(response.statusCode).toBe(200);
        reporter.endStep();

        const responseJson = await response.body;
        const commentsId = await responseJson
            .map((record: { id: number }) => record.id);
        const isSortedDesc = await commentsId
            .slice(1)
            .every((currentId: number, index: number) =>
                commentsId[index] > currentId);

        reporter.startStep("Response should have comments sorted by id in descending order");
        expect(isSortedDesc).toBeTruthy()
        reporter.endStep();
    });
});

describe("Verify resources with authorization", () => {
    beforeEach(() => {
        reporter
            .epic("Interview")
            .feature("Authorization")
            .severity(Severity.Critical);
    });
    it("Register user", async () => {
        reporter
            .story("Register");

        const name = faker.name.firstName();
        const userName = faker.internet.userName(name);
        const email = faker.internet.email(name);
        const password = faker.internet.password();

        const auth = await request(baseUrl)
            .post("/register").send({
                name: name,
                userName: userName,
                email: email,
                password: password
            });
        reporter.startStep("Registration response should have 201 status code");
        expect(auth.statusCode).toBe(201);
        reporter.endStep();

        reporter.startStep("Access token should be present");
        expect(auth.body.accessToken.length).toBeGreaterThan(0);
        reporter.endStep();

        accessToken = await auth.body.accessToken;

        await Config.updateProperty("accessToken", accessToken);
    });
    it("Get all users and check 5th", async () => {
        reporter
            .story("Get");

        const response = await request(baseUrl)
            .get("/users")
            .auth(accessToken, {type: "bearer"});

        reporter.startStep("Status code should be 200");
        expect(response.statusCode).toBe(200);
        reporter.endStep();

        let userJson = response.body
            .find((record: { id: any }) =>
                record.id == confUserId);

        let user: User = <User>userJson;
        reporter.startStep(`Geo info for userid ${confUserId} should be valid`);
        expect(user.address.geo.lat).toBe(`${Config.getProperty("userLat")}`);
        expect(user.address.geo.lng).toBe(`${Config.getProperty("userLng")}`);
        reporter.endStep();
    });

    it('Create new post using token', async () => {
        reporter
            .story("Post");

        const postTitle = faker.hacker.verb();
        const postText = faker.hacker.phrase();
        const post = await request(baseUrl)
            .post("/posts")
            .auth(accessToken, {type: "bearer"})
            .send({title: postTitle, text: postText});

        reporter.startStep("Status code should be 201");
        expect(post.statusCode).toBe(201);
        reporter.endStep();

        reporter.startStep("Post id should be present id response");
        expect(await post.body.id).toBeGreaterThan(0);
        reporter.endStep()

        const postId = await post.body.id;

        const response = await request(baseUrl)
            .get("/posts")
            .query({id: postId});

        reporter.startStep("Post could be found by id");
        expect(response.statusCode).toBe(200);
        reporter.endStep();
        const responseJson = await response.body[0];

        reporter.startStep("Post title and text should be same as created");
        expect(responseJson.title).toBe(postTitle);
        expect(responseJson.text).toBe(postText);
        reporter.endStep();
    })
});